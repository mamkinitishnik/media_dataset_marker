export const isNaturalNumber = (str: string) => {
  var pattern = /^(0|([1-9]\d*))$/;

  return pattern.test(str);
};
