import { DataVersions } from "../data/dataTypes";
import { Filter } from "../filter/filterTypes";
import { Pagination } from "../pagination/paginationTypes";

export type State = {
  data: null | DataVersions;
  filter: Filter;
  pagination: Pagination;
};
