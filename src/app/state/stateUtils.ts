import React from "react";
import { State } from "./stateTypes";
import { reduceToNextData } from "../data/dataUtils/dataReducers";
import { Action } from "../action/actionTypes";
import { createFilter, reduceToNextFilter } from "../filter/filterUtils";
import {
  createPagination,
  reduceToNextPagination,
} from "../pagination/paginationUtils";

export const createState = (): State => ({
  data: null,
  filter: createFilter(),
  pagination: createPagination(),
});

export const StateContext = React.createContext<State>(createState());

export const reduceToNextState = (prevState: State, action: Action): State => {
  const nextState = {
    data: reduceToNextData(prevState.data, action),
    filter: reduceToNextFilter(prevState.filter, action),
    pagination: reduceToNextPagination(prevState.pagination, action),
  };

  if (process.env.NODE_ENV === "development") {
    console.log(action, { prevState, nextState });
  }

  return nextState;
};
