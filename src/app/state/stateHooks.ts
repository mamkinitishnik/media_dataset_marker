import React from "react";
import { StateContext } from "./stateUtils";

export const useState = () => {
  return React.useContext(StateContext);
};
