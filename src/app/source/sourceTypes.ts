import { DataRow } from "../data/dataTypes";

export type Source = {
  fileName: string;
  fileData: DataRow[];
};
