import React from "react";
import { useActionDispatcher } from "../action/actionHooks";
import "./paginationComponent.css";

type Props = {
  totalNumber: number;
  pageNumber: number;
  nextUnmarkedRowIndex?: number;
  prevUnmarkedRowIndex?: number;
};

export const PaginationComponent: React.FC<Props> = (props) => {
  const {
    totalNumber,
    pageNumber,
    prevUnmarkedRowIndex,
    nextUnmarkedRowIndex,
  } = props;

  const dispatch = useActionDispatcher();

  const handleNextClick = React.useCallback(() => {
    dispatch({
      name: "Pagination__ToNextArticle",
    });
  }, [dispatch]);

  const handleNextUnmarkedClick = React.useCallback(() => {
    if (nextUnmarkedRowIndex !== undefined) {
      dispatch({
        name: "Pagination__ToNextUnmarkedArticle",
        pageIndex: nextUnmarkedRowIndex,
      });
    }
  }, [dispatch, nextUnmarkedRowIndex]);

  const handlePrevClick = React.useCallback(() => {
    dispatch({
      name: "Pagination__ToPrevArticle",
    });
  }, [dispatch]);

  const handlePrevUnmarkedClick = React.useCallback(() => {
    if (prevUnmarkedRowIndex !== undefined) {
      dispatch({
        name: "Pagination__ToPrevUnmarkedArticle",
        pageIndex: prevUnmarkedRowIndex - 1,
      });
    }
  }, [dispatch, prevUnmarkedRowIndex]);

  return (
    <div className="pagination__wrapper">
      <div className="pagination__button">
        <button
          title="К предыдущей неразмеченной статье"
          disabled={prevUnmarkedRowIndex === undefined}
          onClick={handlePrevUnmarkedClick}
        >
          {"<<"}
        </button>
        <button
          title="К предыдущей статье"
          disabled={pageNumber <= 1}
          onClick={handlePrevClick}
        >
          {"<"}
        </button>
      </div>
      <div className="pagination__pagesCount">
        {pageNumber}/{totalNumber}
      </div>
      <div className="pagination__button">
        <button
          title="К следующей статье"
          disabled={pageNumber === totalNumber}
          onClick={handleNextClick}
        >
          {">"}
        </button>
        <button
          title="К следующей неразмеченной статье"
          disabled={nextUnmarkedRowIndex === undefined}
          onClick={handleNextUnmarkedClick}
        >
          {">>"}
        </button>
      </div>
    </div>
  );
};
