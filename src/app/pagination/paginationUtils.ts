import { Action } from "../action/actionTypes";
import { createDataLense } from "../data/dataUtils/dataLenses";
import { PAGINATION__DEFAULT_PAGE_INDEX } from "./paginationConstants";
import { Pagination } from "./paginationTypes";

export const createPagination = (): Pagination => ({
  pageIndex: PAGINATION__DEFAULT_PAGE_INDEX,
});

export const reduceToNextPagination = (
  prev: Pagination,
  action: Action
): Pagination => {
  switch (action.name) {
    case "ReaderScreen__ReadComplete":
      const unmarkedRowIndex = createDataLense(
        action.data.patched
      ).selectNextUnmarkedBodyRowIndex();

      if (unmarkedRowIndex !== undefined) {
        return {
          ...prev,
          pageIndex: unmarkedRowIndex,
        };
      }

      return prev;
    case "FilterFormAction__Submit":
      return createPagination();
    case "Pagination__ToNextArticle": {
      return {
        ...prev,
        pageIndex: prev.pageIndex + 1,
      };
    }
    case "Pagination__ToNextUnmarkedArticle": {
      return {
        ...prev,
        pageIndex: action.pageIndex,
      };
    }
    case "Pagination__ToPrevArticle": {
      return {
        ...prev,
        pageIndex: prev.pageIndex - 1,
      };
    }
    case "Pagination__ToPrevUnmarkedArticle": {
      return {
        ...prev,
        pageIndex: action.pageIndex,
      };
    }
    default:
      return prev;
  }
};
