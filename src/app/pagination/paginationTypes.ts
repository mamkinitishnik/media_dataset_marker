export type Pagination = {
  pageIndex: number;
};

type Pagination__ToPrevArticle = {
  name: "Pagination__ToPrevArticle";
};

type Pagination__ToPrevUnmarkedArticle = {
  name: "Pagination__ToPrevUnmarkedArticle";
  pageIndex: number;
};

type Pagination__ToNextArticle = {
  name: "Pagination__ToNextArticle";
};

type Pagination__ToNextUnmarkedArticle = {
  name: "Pagination__ToNextUnmarkedArticle";
  pageIndex: number;
};

export type PaginationAction =
  | Pagination__ToPrevArticle
  | Pagination__ToPrevUnmarkedArticle
  | Pagination__ToNextArticle
  | Pagination__ToNextUnmarkedArticle;
