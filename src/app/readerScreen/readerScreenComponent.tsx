import React from "react";
import { useActionDispatcher } from "../action/actionHooks";
import { dataService } from "../data/dataService";
import "./readerScreenComponent.css";

export function ReaderScreenComponent() {
  const [pending, setPending] = React.useState(false);

  const dispatch = useActionDispatcher();

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const {
        target: { files },
      } = event;

      if (files) {
        setPending(true);

        const [file] = Array.from(files);

        dataService.readFromFile(file).then((data) => {
          dispatch({
            name: "ReaderScreen__ReadComplete",
            data,
          });
        });
      }
    },
    [dispatch]
  );

  if (pending) {
    return (
      <div className="readerScreen">
        <div className="readerScreen__content">
          <h3>Подождите...</h3>
        </div>
      </div>
    );
  }

  return (
    <div className="readerScreen">
      <div className="readerScreen__content">
        <h1>Выберите CSV файл с таблицей данных</h1>
        <p>
          Таблица должна содержать как минимум следующие колонки в начале:
          "[empty],name,portal,text,link,name,date".
          <br />
          <br />
          Колонки категорий будут добавлены в конец автоматически при первом
          открытии файла. Желательно его сразу же сохранить.
        </p>
        <br />
        <input type="file" onChange={handleChange} />
      </div>
    </div>
  );
}
