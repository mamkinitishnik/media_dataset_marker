import { DataVersions } from "../data/dataTypes";

type ReaderScreen__ReadComplete = {
  name: "ReaderScreen__ReadComplete";
  data: DataVersions;
};

export type ReaderScreenAction = ReaderScreen__ReadComplete;
