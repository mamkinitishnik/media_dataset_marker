export type Category = {
  id: string;
  name: string;
  subCategories: CategorySubCategory[];
  description?: string;
};

export type CategorySubCategory = {
  id: string;
  name: string;
};

type CategoriesFormAction__ToggleSubCategory = {
  name: "CategoriesFormAction__ToggleSubCategory";
  articleId: string;
  category: string;
  subCategory: string;
};

export type CategoriesFormAction = CategoriesFormAction__ToggleSubCategory;
