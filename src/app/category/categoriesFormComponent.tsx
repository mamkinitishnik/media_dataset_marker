import React from "react";
import { categories } from "./categoryConstants";
import { CategoryFieldsetComponent } from "./categoryFieldsetComponent";
import "./categoriesFormComponent.css";

type Props = {
  articleId: string;
};

export const CategoriesFormComponent: React.FC<Props> = (props) => {
  const { articleId } = props;

  const wrapperRef = React.useRef<HTMLDivElement>(null);

  React.useLayoutEffect(() => {
    const { current: wrapperNode } = wrapperRef;

    if (wrapperNode) {
      wrapperNode.scrollTo({ top: 0 });
    }
  }, [articleId]);

  return (
    <div ref={wrapperRef} className="categoriesForm__wrapper">
      {categories.map((category) => (
        <CategoryFieldsetComponent
          category={category}
          articleId={articleId}
          key={category.id}
        />
      ))}
    </div>
  );
};
