import { createUniqueId } from "../../util/uniqueId/uniqueIdUtils";
import { Category, CategorySubCategory } from "./categoryTypes";

export const createCategory = (params: {
  name: string;
  subCategories: CategorySubCategory[];
  description?: string;
}): Category => ({
  id: createUniqueId(),
  ...params,
});

export const createSubCategory = (name: string): CategorySubCategory => ({
  id: createUniqueId(),
  name,
});

export const createCategoryValueConstructor = (categoryValue: string) => ({
  toggleSubCategory(subCategory: string) {
    const subCategories = categoryValue.split(",");

    if (subCategories.includes(subCategory)) {
      return subCategories.filter((value) => value !== subCategory).join(",");
    }

    return [...subCategories, subCategory].join(",");
  },
});
