import React from "react";
import { Category } from "./categoryTypes";
import "./categoryFieldsetComponent.css";
import { useActionDispatcher } from "../action/actionHooks";
import { useArticleSelectedSubCategoryNames } from "../article/articleHooks";

type Props = {
  articleId: string;
  category: Category;
};

export const CategoryFieldsetComponent: React.FC<Props> = (props) => {
  const { category, articleId } = props;

  const dispatch = useActionDispatcher();

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { name, value } = event.currentTarget;

      dispatch({
        name: "CategoriesFormAction__ToggleSubCategory",
        articleId,
        category: name,
        subCategory: value,
      });
    },
    [articleId, dispatch]
  );

  const selectedSubCategoryNames = useArticleSelectedSubCategoryNames(
    articleId,
    category.name
  );

  return (
    <div className="categoryFieldset__wrapper">
      <div className="categoryFieldset__title">
        <b>
          {category.name}
          {category.description ? ` (${category.description})` : ""}
        </b>
      </div>
      {category.subCategories.map((subCategory) => (
        <label className="categoryFieldset__label" key={subCategory.name}>
          <input
            type="checkbox"
            name={category.name}
            value={subCategory.name}
            onChange={handleChange}
            checked={selectedSubCategoryNames.includes(subCategory.name)}
          />
          {subCategory.name}
        </label>
      ))}
    </div>
  );
};
