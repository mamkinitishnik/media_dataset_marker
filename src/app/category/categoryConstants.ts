import { Category } from "./categoryTypes";
import { createCategory, createSubCategory } from "./categoryUtils";

const categories: Category[] = [];

categories.push(
  createCategory({
    name: "Другое",
    subCategories: [createSubCategory("да")],
  })
);

categories.push(
  createCategory({
    name: "Дубляж",
    subCategories: [
      createSubCategory("текст совпадает полностью"),
      createSubCategory("текст совпадает частично"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Забастовки",
    subCategories: [
      createSubCategory("итальянские – скрытые"),
      createSubCategory("классические – отказ от работы"),
      createSubCategory("захват предприятий"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Марши",
    subCategories: [
      createSubCategory("общие"),
      createSubCategory(
        "«частные» (женские, пенсионеров, студенческие и т.д.)"
      ),
      createSubCategory("районные"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Другие протестные действия",
    subCategories: [
      createSubCategory("митинги / массовые мероприятия"),
      createSubCategory("цепочки солидарности"),
      createSubCategory("перекрытие дорог"),
      createSubCategory("БЧБ-ленточки"),
      createSubCategory("БЧБ-раскраски"),
      createSubCategory("использование флага"),
      createSubCategory("муралы (граффити, рисунки)"),
      createSubCategory("листовки"),
      createSubCategory("теги (надписи, лозунги)"),
      createSubCategory("лозунги, кричалки"),
      createSubCategory("плакаты"),
      createSubCategory("автопробеги"),
      createSubCategory("голодовки"),
      createSubCategory("посты в социальных сетях"),
      createSubCategory("сидячие демонстрации"),
      createSubCategory("самосожжения"),
      createSubCategory("увольнение в знак протеста"),
      createSubCategory("сигналы машин"),
      createSubCategory("песни (муры)"),
      createSubCategory("призывы к действию"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Репрессии",
    subCategories: [
      createSubCategory("задержания"),
      createSubCategory("аресты"),
      createSubCategory("штрафы"),
      createSubCategory("освобождения"),
      createSubCategory("вызовы на беседы"),
      createSubCategory("увольнения"),
      createSubCategory("угрозы увольнений"),
      createSubCategory("принуждение к увольнение по собственному желанию"),
      createSubCategory("пытки"),
      createSubCategory("избиения"),
      createSubCategory("убийства"),
      createSubCategory("обыск"),
      createSubCategory("разгон митингующих"),
      createSubCategory("суды"),
      createSubCategory("сутки"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Дворовые акции",
    subCategories: [
      createSubCategory("концерты"),
      createSubCategory("видеомосты"),
      createSubCategory("чаепития"),
      createSubCategory("театральные постановки"),
      createSubCategory("прочие перформасы"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Корпоративные действия",
    subCategories: [
      createSubCategory(
        "открытые обращения от корпоративных сообществ (медиков, юристов, ученых и т.д.)"
      ),
      createSubCategory("конкретные действия корпоративных сообществ"),
      createSubCategory(
        "видеообращения корпоративных сообществ (коллективные)"
      ),
      createSubCategory(
        "видеообращения коропоративных сообществ (индивидуальные)"
      ),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Международный уровень",
    subCategories: [
      createSubCategory("акции поддержки беларусов за рубежом"),
      createSubCategory("акции поддержки от небеларусов-людей"),
      createSubCategory(
        "акции поддержки от организаций (профсоюзы, сообщества)"
      ),
      createSubCategory("акции / мероприятия поддержки от стран"),
      createSubCategory("высказывания политиков из других стран"),
      createSubCategory("высказывания власти о других странах"),
      createSubCategory("санкции против других стран"),
      createSubCategory("санкции против власти"),
      createSubCategory("международный диалог с участием власти"),
      createSubCategory("международный диалог с участием оппозиции"),
      createSubCategory("международный диалог без участия Беларуси"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Преступления",
    description: "если в статье событие описывается именно как преступление",
    subCategories: [
      createSubCategory("вандализм"),
      createSubCategory("драка, силовые столкновени"),
      createSubCategory("угроза взрыва, сообщение о минировании"),
      createSubCategory("огнестрельное оружие"),
      createSubCategory("холодное оружие"),
      createSubCategory("насилие с применением подручных средств"),
      createSubCategory("насилие с примирением специальных средств"),
      createSubCategory("призыв к захвату власти"),
      createSubCategory("угроза применения насилия"),
      createSubCategory("насилие против представителя власти"),
      createSubCategory("оскорбления"),
      createSubCategory("применение насилия против силовиков"),
      createSubCategory("массовые беспорядки"),
      createSubCategory("смерть человека"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Взаимопомощь",
    subCategories: [
      createSubCategory("сбор средств"),
      createSubCategory("благотворительность"),
      createSubCategory("покупка в знак солидарности"),
      createSubCategory("трудоустройство"),
      createSubCategory("переквалификация"),
      createSubCategory("выезд за рубеж"),
      createSubCategory("транспортные услуги"),
      createSubCategory("медицинская помощь"),
      createSubCategory("другое"),
    ],
    description: "отмечаем тогда, когда белорусы помогают белорусам",
  })
);

categories.push(
  createCategory({
    name: "Пропаганда",
    subCategories: [
      createSubCategory("пророссийская"),
      createSubCategory("пролукашенковская"),
      createSubCategory("прооппозиционная"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Коллективная мобилизация",
    subCategories: [
      createSubCategory("комитеты"),
      createSubCategory("стачкомы"),
      createSubCategory("штабы"),
      createSubCategory("советы (местные, национальные)"),
      createSubCategory("инициативы"),
      createSubCategory("создание иных организаций"),
      createSubCategory("создание / объявления о создании партий"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Упоминания чатов",
    subCategories: [
      createSubCategory("дворовые чаты"),
      createSubCategory("профессиональные чаты"),
      createSubCategory("чаты водителей"),
      createSubCategory("городские чаты"),
      createSubCategory("ситуативные протестные чаты"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Упоминание Великой Отечественной войны",
    subCategories: [createSubCategory("да")],
  })
);

categories.push(
  createCategory({
    name: "Протестная повестка (против чего)",
    subCategories: [
      createSubCategory("деятельность правительства в рамках их компетенций"),
      createSubCategory("политические заключенные"),
      createSubCategory("трудовые отношения"),
      createSubCategory("репрессии"),
      createSubCategory("культура"),
      createSubCategory("covid-19"),
      createSubCategory("социальная политика"),
      createSubCategory("доход"),
      createSubCategory("собственность"),
      createSubCategory("энергоносители"),
      createSubCategory("антимайдан"),
      createSubCategory("выборы"),
      createSubCategory("реформа конституции"),
      createSubCategory("союз с россией"),
      createSubCategory("беззаконие"),
      createSubCategory("земельные вопросы"),
      createSubCategory(
        "ограничение конституционных прав (слова, высказывания)"
      ),
      createSubCategory("гендерное неравенство"),
      createSubCategory("коррупция"),
      createSubCategory("окружающая среда"),
      createSubCategory("этническая / национальная повестка"),
      createSubCategory("репрессии (ненасильственные)"),
      createSubCategory("насилие физическое (пытки, избиения, убийства)"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Против кого?",
    subCategories: [
      createSubCategory("президент"),
      createSubCategory("правительство"),
      createSubCategory("местные власти"),
      createSubCategory("другие государства"),
      createSubCategory("силовики"),
      createSubCategory("бизнес"),
      createSubCategory("суды"),
      createSubCategory("отдельные личности"),
      createSubCategory("организации"),
      createSubCategory("оппозиция"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Реакция властей",
    subCategories: [
      createSubCategory("обещания"),
      createSubCategory("реформы"),
      createSubCategory("насилие"),
      createSubCategory("аресты"),
      createSubCategory("проверки"),
      createSubCategory("закрытие бизнеса"),
      createSubCategory("предупреждения"),
      createSubCategory("угрозы применения силы"),
      createSubCategory("гражданский диалог"),
      createSubCategory("психологическое давление"),
      createSubCategory("осуждение"),
      createSubCategory("критика"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Провластные мероприятия",
    subCategories: [
      createSubCategory("митинги в поддержку власти"),
      createSubCategory("совещания"),
      createSubCategory("открытия объектов"),
      createSubCategory("посещения предприятий"),
      createSubCategory("провластные автопробеги"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Выборы",
    subCategories: [
      createSubCategory("цик"),
      createSubCategory("уик"),
      createSubCategory("фальсификации"),
      createSubCategory("агитация"),
      createSubCategory("процедура голосования"),
      createSubCategory("подсчет голосов"),
      createSubCategory("нарушения"),
      createSubCategory("наблюдатели"),
      createSubCategory("итоги голосования"),
      createSubCategory("досрочное голосование"),
      createSubCategory("основной день голосования"),
      createSubCategory("надомное голосование"),
      createSubCategory("сбор и подсчет подписей"),
      createSubCategory("регистрация кандидатов"),
      createSubCategory("репрессии против кандидатов"),
      createSubCategory("информация о кандидатах и их программах"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Создание мемориалов",
    subCategories: [
      createSubCategory("цветы"),
      createSubCategory("разрушение мемориалов"),
      createSubCategory("фотографии"),
      createSubCategory("восстановление, обновление"),
      createSubCategory("обновление"),
      createSubCategory("запрет мемориалов"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Кто говорит?",
    subCategories: [
      createSubCategory("высказывание политиков"),
      createSubCategory("эксперты (политологи и т.д)"),
      createSubCategory("высказывания обычных людей"),
      createSubCategory("высказывания сотрудников органов правопорядка"),
      createSubCategory("представители оппозиции"),
      createSubCategory("лукашенко"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Конституция",
    subCategories: [
      createSubCategory("новая конституция"),
      createSubCategory("конституция 1994"),
      createSubCategory("нынешняя конституция"),
      createSubCategory("предложения правок"),
      createSubCategory("оставить как есть"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Зарубежные сми о протестах",
    subCategories: [
      createSubCategory("европа"),
      createSubCategory("сша"),
      createSubCategory("россия"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Действия властей во время протестов",
    subCategories: [
      createSubCategory("сокрытие номеров"),
      createSubCategory("использование госсимволики"),
      createSubCategory("сокрытие личности (балаклавы, маски)"),
      createSubCategory("угрозы"),
      createSubCategory("защита правительственных зданий"),
      createSubCategory("ограждения (проволока)"),
      createSubCategory("применения спецсредств"),
      createSubCategory("предупреждения"),
      createSubCategory("применение оружия"),
      createSubCategory("использование физической силы"),
      createSubCategory("громкая музыка"),
      createSubCategory("закрытие/открытие метро"),
      createSubCategory("блокировка сайтов"),
      createSubCategory("отключение/включение интернета"),
      createSubCategory("блокировка мессенджеров"),
      createSubCategory("другое"),
    ],
  })
);

categories.push(
  createCategory({
    name: "Юмор",
    subCategories: [
      createSubCategory("мемы"),
      createSubCategory("шуточные посты"),
      createSubCategory("другое"),
    ],
  })
);

export { categories };
