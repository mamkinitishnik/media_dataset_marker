import React from "react";

import "./customFieldset.css";

type Props = {
  legend: React.ReactNode;
  content: React.ReactNode;
};

export const CustomFieldset = (props: Props) => {
  const { legend, content } = props;

  return (
    <div className="customFieldset__wrapper">
      <div className="customFieldset__legend">{legend}</div>
      <div className="customFieldset__content">{content}</div>
    </div>
  );
};
