import React from "react";
import { StoreComponent } from "./store/storeComponent";
import { ViewComponent } from "./view/viewComponent";

export const AppComponent: React.FC = () => {
  return (
    <StoreComponent>
      <ViewComponent />
    </StoreComponent>
  );
};
