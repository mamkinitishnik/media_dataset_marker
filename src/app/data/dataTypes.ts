export type DataRow = string[];

export type DataVersions = {
  initial: DataRow[];
  patched: DataRow[];
};

export type DataService = {
  readFromFile(file: File): Promise<DataVersions>;
  writeToFile(data: DataRow[]): Promise<void>;
};

export type DataLense = {
  selectRow(index: number): undefined | DataRow;
  selectByTextMatch(text: string): DataRow[];
  selectUnmarked(): DataRow[];
  selectColumnIndex(columnName: string): undefined | number;
  selectRowIndex(recordId: string): undefined | number;
  selectValue(recordId: string, columnName: string): undefined | string;
  selectNextUnmarkedBodyRowIndex(baseIndex?: number): undefined | number;
  selectPrevUnmarkedRowIndex(baseIndex?: number): undefined | number;
};

export type DataConstructor = {
  get(): DataRow[];
  createColumn(columnName: string): DataConstructor;
  setValue(
    rowIndex: number,
    columnName: string,
    value: string
  ): DataConstructor;
  toggleSubCategory(
    articleId: string,
    category: string,
    subCategory: string
  ): DataConstructor;
};
