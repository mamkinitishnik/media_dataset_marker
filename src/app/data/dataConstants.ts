export const DATA__ID_COLUMN_INDEX = 0;

export const DATA__PORTAL_COLUMN_INDEX = 2;

export const DATA__TEXT_COLUMN_INDEX = 3;

export const DATA__LINK_COLUMN_INDEX = 4;

export const DATA__DATE_COLUMN_INDEX = 5;
