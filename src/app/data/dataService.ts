import Papa from "papaparse";
import { categories } from "../category/categoryConstants";
import { DataError } from "../table/tableUtils";
import { DataService } from "./dataTypes";
import { createDataConstructor } from "./dataUtils/dataCreators";
import { normalizeToData } from "./dataUtils/dataNormalizers";

let fileName = "data.csv";

export const dataService: DataService = {
  async readFromFile(file) {
    return new Promise((resolve, reject) => {
      fileName = file.name;

      Papa.parse(file, {
        complete(result) {
          const { data } = result;

          const normalizedData = normalizeToData(data);

          const patchedData = categories
            .reduce((acc, category) => {
              return acc.createColumn(category.name);
            }, createDataConstructor(normalizedData))
            .get();

          if (data) {
            resolve({ initial: normalizedData, patched: patchedData });
          } else {
            reject(new DataError("Failed to read source file."));
          }
        },
      });
    });
  },
  async writeToFile(data) {
    const unparsed = Papa.unparse(data);

    var blob = new Blob([unparsed], { type: "text/csv;charset=utf-8;" });

    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, fileName);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        link.style.visibility = "hidden";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  },
};
