import { DataError } from "../../table/tableUtils";
import { DataConstructor, DataRow } from "../dataTypes";
import { createDataLense } from "./dataLenses";

export const createDataConstructor = (data: DataRow[]): DataConstructor => ({
  get() {
    return data;
  },
  createColumn(name) {
    let nextData = data;

    const exists = createDataLense(data).selectColumnIndex(name) !== undefined;

    if (!exists) {
      nextData = data.map((row, index) => {
        if (index === 0) {
          const nextRow = [...row, name];

          return nextRow;
        } else {
          const nextRow = [...row, ""];

          return nextRow;
        }
      });
    }

    return createDataConstructor(nextData);
  },
  setValue(rowIndex, columnName, value) {
    const columnIndex = createDataLense(data).selectColumnIndex(columnName);

    if (columnIndex === undefined) {
      throw new DataError("Failed to set value.");
    }

    const targetRow = data[rowIndex];

    const nextRow = [...targetRow];

    nextRow[columnIndex] = value;

    const nextData = [...data];

    nextData[rowIndex] = nextRow;

    return createDataConstructor(nextData);
  },
  toggleSubCategory(recordId, category, subCategory) {
    const dataLense = createDataLense(data);

    const columnIndex = dataLense.selectColumnIndex(category);

    const rowIndex = dataLense.selectRowIndex(recordId);

    if (rowIndex === undefined) {
      throw new DataError(`No record with such id: ${recordId}.`);
    }

    if (columnIndex === undefined) {
      throw new DataError(`No such category: ${category}.`);
    }

    const nextData = [...data];

    nextData[rowIndex] = [...nextData[rowIndex]];

    const prevValue = nextData[rowIndex][columnIndex].split("|");

    if (prevValue.includes(subCategory)) {
      nextData[rowIndex][columnIndex] = prevValue
        .filter((sc) => sc !== subCategory)
        .join("|");
    } else {
      nextData[rowIndex][columnIndex] = [...prevValue, subCategory].join("|");
    }

    return createDataConstructor(nextData);
  },
});
