import { DataLense, DataRow } from "../dataTypes";
import { DATA__ID_COLUMN_INDEX } from "../dataConstants";
import { createTableLense, DataError } from "../../table/tableUtils";
import { categories } from "../../category/categoryConstants";

export const createDataLense = (data: DataRow[]): DataLense => ({
  selectRow(index) {
    return data.slice(index, index + 1)[0];
  },
  selectByTextMatch(text) {
    const bodyRows = data.slice(1);

    const filteredRows = bodyRows.filter((row) => {
      const words = text.split(" ");

      const rowText = JSON.stringify(row).toLowerCase();

      for (let word of words) {
        if (!rowText.includes(word.toLowerCase())) {
          return false;
        }
      }

      return true;
    });

    const result: DataRow[] = [...data.slice(0, 1), ...filteredRows];

    return result;
  },
  selectUnmarked() {
    return data.filter((row) => {
      for (let category of categories) {
        const columnIndex = createDataLense(data).selectColumnIndex(
          category.name
        );

        if (columnIndex !== undefined) {
          const value = row[columnIndex];

          if (value) {
            return false;
          }
        }
      }

      return true;
    });
  },
  selectColumnIndex(columnName) {
    const index = data[0].indexOf(columnName);

    if (index > -1) {
      return index;
    }
  },
  selectRowIndex(recordId) {
    const index = data.findIndex(
      (row) => row[DATA__ID_COLUMN_INDEX] === recordId
    );

    if (index > -1) {
      return index;
    }
  },
  selectValue(recordId, columnName) {
    const rowIndex = createDataLense(data).selectRowIndex(recordId);

    const columnIndex = createDataLense(data).selectColumnIndex(columnName);

    if (rowIndex === undefined || columnIndex === undefined) {
      throw new DataError(`Failed to select data value for record ${recordId}`);
    }

    return createTableLense(data).selectValue({
      rowIndex,
      columnIndex,
    });
  },
  selectNextUnmarkedBodyRowIndex(baseIndex = 0) {
    const baseData = data.slice(baseIndex);

    const index = baseData.findIndex((row) => {
      for (let category of categories) {
        const columnIndex = createDataLense(data).selectColumnIndex(
          category.name
        );

        if (columnIndex !== undefined) {
          const value = row[columnIndex];

          if (value) {
            return false;
          }
        }
      }

      return true;
    });

    return index > -1 ? baseIndex + index : undefined;
  },
  selectPrevUnmarkedRowIndex(baseIndex = 0) {
    const baseData = data.slice(1, baseIndex);

    const index = baseData.reverse().findIndex((row) => {
      for (let category of categories) {
        const columnIndex = createDataLense(data).selectColumnIndex(
          category.name
        );

        if (columnIndex !== undefined) {
          const value = row[columnIndex];

          if (value) {
            return false;
          }
        }
      }

      return true;
    });

    return index > -1 ? baseIndex - index : undefined;
  },
});
