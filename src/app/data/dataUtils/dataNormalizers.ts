import { Article } from "../../article/articleTypes";
import { DataRow } from "../dataTypes";
import {
  DATA__ID_COLUMN_INDEX,
  DATA__LINK_COLUMN_INDEX,
  DATA__TEXT_COLUMN_INDEX,
} from "../dataConstants";
import { DataError } from "./dataErrors";

export const normalizeToData = (data: unknown[]): DataRow[] => {
  return data.map((item) => {
    if (Array.isArray(item)) {
      return item.map((value) => {
        if (typeof value === "string") {
          return value;
        }

        throw new DataError("Failed to normalize to data.");
      });
    }

    throw new DataError("Failed to normalize to data.");
  });
};

function reverseString(string: string) {
  return string.split("").reverse().join("");
}

const telegramPathRegExp = /^(?<id>[0-9]+)(?<channel>.+)$/;

const repairLink = (linkValue: string): string => {
  const link = linkValue.replace("https:t.me", "");

  const match = telegramPathRegExp.exec(reverseString(link));

  if (match?.groups) {
    const { channel, id } = match.groups;

    return `https://t.me/${reverseString(channel)}/${reverseString(id)}`;
  }

  return linkValue;
};

export const normalizeDataRowToArticle = (dataRow: DataRow): Article => {
  const linkValue = dataRow[DATA__LINK_COLUMN_INDEX];

  return {
    id: dataRow[DATA__ID_COLUMN_INDEX],
    text: dataRow[DATA__TEXT_COLUMN_INDEX],
    link: repairLink(linkValue),
  };
};
