import { Action } from "../../action/actionTypes";
import { DataVersions } from "../dataTypes";
import { createDataConstructor } from "./dataCreators";

export const reduceToNextData = (
  prev: null | DataVersions,
  action: Action
): null | DataVersions => {
  switch (action.name) {
    case "ReaderScreen__ReadComplete":
      return action.data;
    case "TableScreenHeaderAction__ChangesSaved": {
      if (prev) {
        return {
          ...prev,
          initial: prev.patched,
        };
      }

      return prev;
    }
    case "CategoriesFormAction__ToggleSubCategory":
      if (prev) {
        const nextData = createDataConstructor(prev.patched)
          .toggleSubCategory(
            action.articleId,
            action.category,
            action.subCategory
          )
          .get();

        return { ...prev, patched: nextData };
      }

      return prev;
    default:
      return prev;
  }
};
