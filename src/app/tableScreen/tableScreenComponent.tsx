import React from "react";
import { normalizeDataRowToArticle } from "../data/dataUtils/dataNormalizers";
import { createDataLense } from "../data/dataUtils/dataLenses";
import "./tableScreenComponent.css";
import { useState } from "../state/stateHooks";
import { FilterFormComponent } from "../filter/filterFormComponent";
import { ArticleComponent } from "../article/articleComponent";
import { TableScreenHeaderComponent } from "./tableScreenHeaderComponent";
import { DataRow } from "../data/dataTypes";
import { CategoriesFormComponent } from "../category/categoriesFormComponent";
import { PaginationComponent } from "../pagination/paginationComponent";

type Props = {
  data: DataRow[];
};

export const TableScreenComponent: React.FC<Props> = (props) => {
  const { data } = props;

  const {
    filter: { searchText, submitted },
    pagination: { pageIndex },
  } = useState();

  const [filteredData, setFilteredData] = React.useState<DataRow[]>([]);

  React.useEffect(() => {
    if (submitted) {
      if (searchText) {
        const dataByText = createDataLense(data).selectByTextMatch(searchText);

        setFilteredData(dataByText);
      } else {
        setFilteredData(data);
      }
    }
  }, [data, searchText, submitted]);

  const article = React.useMemo(() => {
    const firstRow = createDataLense(filteredData).selectRow(pageIndex);

    if (firstRow) {
      return normalizeDataRowToArticle(firstRow);
    }

    return null;
  }, [filteredData, pageIndex]);

  const totalNumber = React.useMemo(() => {
    return filteredData.length - 1;
  }, [filteredData.length]);

  const prevUnmarkedRowIndex = React.useMemo(() => {
    return createDataLense(filteredData).selectPrevUnmarkedRowIndex(pageIndex);
  }, [filteredData, pageIndex]);

  const nextUnmarkedRowIndex = React.useMemo(() => {
    return createDataLense(filteredData).selectNextUnmarkedBodyRowIndex(
      pageIndex + 1
    );
  }, [filteredData, pageIndex]);

  const pageNumber = totalNumber <= 0 ? 0 : pageIndex;

  return (
    <div className="tableScreen__wrapper">
      <div className="tableScreen__body">
        <div className="tableScreen__header">
          <TableScreenHeaderComponent />
        </div>
        <div className="tableScreen__filter">
          <FilterFormComponent />
        </div>
        <div className="tableScreen__pagination">
          <PaginationComponent
            prevUnmarkedRowIndex={prevUnmarkedRowIndex}
            nextUnmarkedRowIndex={nextUnmarkedRowIndex}
            pageNumber={pageNumber}
            totalNumber={totalNumber}
          />
        </div>
        {article && (
          <div className="tableScreen__article">
            <ArticleComponent article={article} />
          </div>
        )}
        {article && (
          <div className="tableScreen__categories">
            <CategoriesFormComponent articleId={article.id} />
          </div>
        )}
      </div>
    </div>
  );
};
