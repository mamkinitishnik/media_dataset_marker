type TableScreenHeaderAction__ChangesSaved = {
  name: "TableScreenHeaderAction__ChangesSaved";
};

export type TableScreenHeaderAction = TableScreenHeaderAction__ChangesSaved;
