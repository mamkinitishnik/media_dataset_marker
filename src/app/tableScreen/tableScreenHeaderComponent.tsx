import React from "react";
import { useActionDispatcher } from "../action/actionHooks";
import { dataService } from "../data/dataService";
import { useState } from "../state/stateHooks";
import "./tableScreenHeaderComponent.css";

type Props = {};

export const TableScreenHeaderComponent: React.FC<Props> = (props) => {
  const { data } = useState();

  const dispatch = useActionDispatcher();

  const handleButtonClick = React.useCallback(() => {
    if (data) {
      dataService.writeToFile(data.patched).then(() => {
        dispatch({
          name: "TableScreenHeaderAction__ChangesSaved",
        });
      });
    }
  }, [data, dispatch]);

  return (
    <div className="tableScreenHeader__wrapper">
      <div className="tableScreenHeader__content">
        <div className="tableScreenHeader__message">Media Dataset Marker</div>
        <button
          className="tableScreenHeader__button"
          onClick={handleButtonClick}
        >
          Сохранить изменения
        </button>
      </div>
    </div>
  );
};
