import React from "react";
import { useState } from "../state/stateHooks";
import { ReaderScreenComponent } from "../readerScreen/readerScreenComponent";
import { TableScreenComponent } from "../tableScreen/tableScreenComponent";

export function ViewComponent() {
  const state = useState();

  if (!state.data) {
    return <ReaderScreenComponent />;
  }

  return <TableScreenComponent data={state.data.patched} />;
}
