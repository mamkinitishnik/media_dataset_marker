import React from "react";
import { ActionDispatcherContext } from "../action/actionUtils";
import {
  createState,
  reduceToNextState,
  StateContext,
} from "../state/stateUtils";

export const StoreComponent: React.FC = (props) => {
  const { children } = props;

  const [state, dispatch] = React.useReducer(
    reduceToNextState,
    null,
    createState
  );

  return (
    <ActionDispatcherContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>{children}</StateContext.Provider>
    </ActionDispatcherContext.Provider>
  );
};
