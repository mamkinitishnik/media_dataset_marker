import React from "react";
import { useActionDispatcher } from "../action/actionHooks";
import { CustomFieldset } from "../customFieldset/customFieldset";
import { useState } from "../state/stateHooks";
import "./filterFormComponent.css";

export const FilterFormComponent = () => {
  const { filter } = useState();

  const dispatch = useActionDispatcher();

  const handleSearchChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch({
        name: "FilterFormAction__SearchChange",
        value: event.target.value,
      });
    },
    [dispatch]
  );

  const handleSubmit = React.useCallback(
    (event: React.SyntheticEvent) => {
      event.preventDefault();

      dispatch({
        name: "FilterFormAction__Submit",
      });
    },
    [dispatch]
  );

  return (
    <CustomFieldset
      legend={"Фильтрация"}
      content={
        <form className="filterForm__form" onSubmit={handleSubmit}>
          <label className="filterForm__label">
            Ключевые слова
            <br />
            <input
              value={filter.searchText}
              type="text"
              placeholder="Ключевые слова"
              className="filterForm__input"
              onChange={handleSearchChange}
            />
          </label>

          {!filter.submitted && (
            <div className="filterForm__footer">
              <button type="submit">Применить</button>
            </div>
          )}
        </form>
      }
    />
  );
};
