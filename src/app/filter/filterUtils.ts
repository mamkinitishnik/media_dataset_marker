import { Action } from "../action/actionTypes";
import { Filter } from "./filterTypes";

export const createFilter = (): Filter => ({
  searchText: "",
  submitted: true,
});

export const reduceToNextFilter = (prev: Filter, action: Action): Filter => {
  switch (action.name) {
    case "FilterFormAction__SearchChange":
      return {
        ...prev,
        searchText: action.value,
        submitted: false,
      };
    case "FilterFormAction__Submit":
      return {
        ...prev,
        submitted: true,
      };
    default:
      return prev;
  }
};
