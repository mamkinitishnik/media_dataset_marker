type FilterFormAction__SearchChange = {
  name: "FilterFormAction__SearchChange";
  value: string;
};

type FilterFormAction__Submit = {
  name: "FilterFormAction__Submit";
};

export type FilterFormAction =
  | FilterFormAction__SearchChange
  | FilterFormAction__Submit;

export type Filter = {
  searchText: string;
  submitted: boolean;
};
