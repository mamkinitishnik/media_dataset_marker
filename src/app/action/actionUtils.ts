import React from "react";
import { ActionDispatcher } from "../action/actionTypes";

export const ActionDispatcherContext = React.createContext<ActionDispatcher>(
  (action) => {
    console.log(action);
  }
);
