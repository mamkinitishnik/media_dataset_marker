import { CategoriesFormAction } from "../category/categoryTypes";
import { FilterFormAction } from "../filter/filterTypes";
import { PaginationAction } from "../pagination/paginationTypes";
import { ReaderScreenAction } from "../readerScreen/readerScreenTypes";
import { TableScreenHeaderAction } from "../tableScreen/tableScreenTypes";

export type Action =
  | FilterFormAction
  | ReaderScreenAction
  | PaginationAction
  | CategoriesFormAction
  | TableScreenHeaderAction;

export type ActionDispatcher = (action: Action) => void;
