import React from "react";
import { ActionDispatcherContext } from "./actionUtils";

export const useActionDispatcher = () => {
  return React.useContext(ActionDispatcherContext);
};
