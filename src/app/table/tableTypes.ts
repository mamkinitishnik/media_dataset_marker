export type TableRow = string[];

export type TableCoordinates = {
  rowIndex: number;
  columnIndex: number;
};

export type Table = TableRow[];

export type TableLense = {
  selectRow(index: number): undefined | TableRow;
  selectValue(coordinates: TableCoordinates): undefined | string;
  selectLastRowIndex(): number;
  selectLastColumnIndex(): number;
};

export type TableConstructor = {
  get(): Table;
  setValue(coordinates: TableCoordinates, value: string): TableConstructor;
};
