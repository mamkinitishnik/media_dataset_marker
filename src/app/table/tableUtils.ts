import { Table, TableConstructor, TableLense } from "./tableTypes";

export class TableError extends Error {
  name = "Table Error";
}

export const createTableConstructor = (table: Table): TableConstructor => ({
  get() {
    return table;
  },
  setValue(coordinates, value) {
    const { rowIndex, columnIndex } = coordinates;

    const targetRow = table[rowIndex];

    const nextRow = [...targetRow];

    nextRow[columnIndex] = value;

    const nextTable = [...table];

    nextTable[rowIndex] = nextRow;

    return createTableConstructor(table);
  },
});

export class DataError extends Error {
  name = "Data Error";
}

export const createTableLense = (table: Table): TableLense => ({
  selectRow(index) {
    return table.slice(index, index + 1)[0];
  },
  selectValue(coordinates) {
    const { rowIndex, columnIndex } = coordinates;

    const row = table[rowIndex];

    if (!table) {
      return undefined;
    }

    const value = row[columnIndex];

    if (typeof value !== "string") {
      return undefined;
    }

    return value;
  },
  selectLastRowIndex() {
    return table.length - 1;
  },
  selectLastColumnIndex() {
    return table[0].length - 1;
  },
});
