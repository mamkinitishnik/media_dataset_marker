import React from "react";
import { Article } from "./articleTypes";
import "./articleComponent.css";
import { useArticleMarkedFlag } from "./articleHooks";
import { CustomFieldset } from "../customFieldset/customFieldset";

type Props = {
  article: Article;
};

export const ArticleComponent: React.FC<Props> = (props) => {
  const { article } = props;

  const scrollableRef = React.useRef<HTMLDivElement>(null);

  React.useLayoutEffect(() => {
    const { current: scrollableNode } = scrollableRef;

    if (scrollableNode) {
      scrollableNode.scrollTo({ top: 0 });
    }
  }, [article.id]);

  const marked = useArticleMarkedFlag(article.id);

  return (
    <CustomFieldset
      legend={
        <div className="article__legend">
          <span>Материал {marked && <MarkedIndicator />}</span>
          <a href={article.link} target="_blank" rel="noreferrer">
            Источник
          </a>
        </div>
      }
      content={
        <div className="article__content" ref={scrollableRef}>
          <div className="article__text">{article.text}</div>
        </div>
      }
    ></CustomFieldset>
  );
};

const MarkedIndicator = () => (
  <span className="article__marked-indicator">отмечен</span>
);
