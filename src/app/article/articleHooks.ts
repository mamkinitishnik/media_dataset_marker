import React from "react";
import { categories } from "../category/categoryConstants";
import { createDataLense } from "../data/dataUtils/dataLenses";
import { useState } from "../state/stateHooks";

export const useArticleSelectedSubCategoryNames = (
  articleId: string,
  categoryName: string
): string[] => {
  const { data } = useState();

  return React.useMemo(() => {
    if (data) {
      const { patched: patchedData } = data;

      const value =
        createDataLense(patchedData).selectValue(articleId, categoryName) || "";

      return value.split("|");
    }

    return [];
  }, [articleId, categoryName, data]);
};

export const useArticleMarkedFlag = (articleId: string): boolean => {
  const { data } = useState();

  return React.useMemo(() => {
    if (data) {
      const { patched } = data;

      for (let category of categories) {
        const lense = createDataLense(patched);

        const value = lense.selectValue(articleId, category.name);

        if (!!value) {
          return true;
        }
      }

      return false;
    }

    return false;
  }, [articleId, data]);
};
