export type Article = {
  id: string;
  text: string;
  link: string;
};
